﻿using BepInEx.Configuration;

namespace ChatMiniMod
{
    static class Settings
    {
        public static ConfigEntry<bool> IsShoutKey { get; private set; }
        public static ConfigEntry<string> ShoutKey { get; private set; }
        public static ConfigEntry<bool> IsOnChatVisible { get; private set; }
        public static ConfigEntry<float> OnChatVisibleTime { get; private set; }
        public static void Init()
        {

            IsShoutKey = Plugin.Instance.Config.Bind(
                "shiftShout.isShoutKey",
                "EnableShoutKey",
                true,
                "Enable Shoutkey");

            ShoutKey = Plugin.Instance.Config.Bind(
                "shiftShout.ShoutKey",
                "ShoutKeyCode",
                "LeftShift",
                "Press and Enter to Shout. Check KeyCode https://docs.unity3d.com/ScriptReference/KeyCode.html"
                );

            IsOnChatVisible = Plugin.Instance.Config.Bind(
                "chat.isOnChatVisible",
                "OnChatVisible",
                true,
                "Enable OnNewChat Chat Window Visible");

            OnChatVisibleTime = Plugin.Instance.Config.Bind(
                "chat.ChatHideTime",
                "ChatHideTime",
                3f,
                "Auto Chat Window Hide Time(sec)");
        }
    }
}
