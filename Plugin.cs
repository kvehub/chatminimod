﻿using BepInEx;
using HarmonyLib;
using System;
using UnityEngine;

namespace ChatMiniMod
{
    [BepInPlugin(GUID, MODNAME, VERSION)]
    [BepInProcess("valheim.exe")]
    public class Plugin : BaseUnityPlugin
    {
        public const string
            MODNAME = "ChatMiniMod",
            AUTHOR = "kve",
            GAME = "valheim.client",
            GUID = "com." + AUTHOR + "." + GAME + "." + MODNAME,
            VERSION = "1.0.0.0";
        public static Plugin Instance { get; private set; }
        public static BepInEx.Logging.ManualLogSource logger => Instance.Logger;
        public static BepInEx.Configuration.ConfigFile config => Instance.Config;

        public static KeyCode shoutKey;
        void Awake()
        {
            Instance = this;
            Settings.Init();

            if (Settings.IsShoutKey.Value)
            {
                if (Enum.TryParse(Settings.ShoutKey.Value, out KeyCode sk))
                {
                    shoutKey = sk;
                    Harmony.CreateAndPatchAll(typeof(patch.ShiftShout), GUID);
                }
                else
                {
                    Logger.LogError("There is a problem with the key settings.");
                }
            }

            if (Settings.IsOnChatVisible.Value)
            {
                Harmony.CreateAndPatchAll(typeof(patch.PatchChatShow), GUID);
            }
        }
    }
}
