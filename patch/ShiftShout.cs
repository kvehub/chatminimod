﻿using HarmonyLib;
using System;
using UnityEngine;

namespace ChatMiniMod.patch
{
    class ShiftShout
    {
        [HarmonyPrefix]
        [HarmonyPatch(typeof(Chat), "SendText")]
        static void PatchDetectKey(ref Talker.Type type, string text)
        {
            if (type == Talker.Type.Normal)
                if (Input.GetKey(Plugin.shoutKey))
                    type = Talker.Type.Shout;
        }
    }
}
