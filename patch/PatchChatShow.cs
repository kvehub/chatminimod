﻿using HarmonyLib;

namespace ChatMiniMod.patch
{
    class PatchChatShow
    {
        [HarmonyPostfix]
        [HarmonyPatch(typeof(Chat), "Awake")]
        static void PatchHideDelay(ref float ___m_hideDelay)
        {
            ___m_hideDelay = Settings.OnChatVisibleTime.Value;
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(Chat), "OnNewChatMessage")]
        static void PatchOnNewChat(ref float ___m_hideTimer)
        {
            ___m_hideTimer = 0f;
        }
    }
}
